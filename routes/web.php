<?php

Route::get('/', 'IndexController@index');
Route::post('/mnps/search', 'IndexController@search')->name('mnps.search.ajax');

Route::get('/admin/login', 'Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Auth\LoginController@login');
Route::any('/admin/logout', 'Auth\LoginController@logout')->name('admin.logout');

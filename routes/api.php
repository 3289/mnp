<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/register', 'Api\AuthController@register');
Route::post('/login', 'Api\AuthController@authenticate');
Route::get('/getpointinfo', 'Api\FapController@get');
Route::get('/getpoints', 'Api\FapController@index');
Route::get('/getregions', 'Api\FapController@getregions');
Route::post('/estimateobject', 'Api\FapController@addComments');
Route::post('/sendrequest', 'Api\FapController@sendRequest');
Route::get('/getpointsbytype', 'Api\FapController@byType');

Route::get('/getmnpinfo', 'Api\FapController@getMnpInfo');
Route::get('/getfapinfo', 'Api\FapController@getFapInfo');
Route::post('/updateinfo', 'Api\AuthController@update');

Route::get('/getmyrequests', 'Api\FapController@getMyRequest');

Route::get('/gethealth', 'IndexController@index')->name('home');

Route::get('/user', function (Request $request) {
    return $request->user();
});

<form method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="pwd">Новый пароль:</label>
        <input type="password" class="form-control" id="pwd" name="password">
    </div>
    <button type="submit" class="btn btn-success">Сохранить</button>
</form>
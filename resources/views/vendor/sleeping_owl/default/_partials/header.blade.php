<a href="{{ url(config('sleeping_owl.url_prefix')) }}" class="logo">
	<span class="logo-lg">{!! AdminTemplate::getLogo() !!}</span>
	<span class="logo-mini">{!! AdminTemplate::getLogoMini() !!}</span>
</a>

<nav class="navbar navbar-static-top" role="navigation">
	<!-- Sidebar toggle button-->
	<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
	</a>
	<div class="navbar-custom-menu">
		<ul class="nav navbar-nav">
			@stack('navbar')
		</ul>

		<ul class="nav navbar-nav navbar-right">
			@stack('navbar.right')
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">{{\Auth::user()->name}}
					<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="{{route('admin.logout')}}">Выйти</a></li>
				</ul>
			</li>
		</ul>
	</div>
</nav>

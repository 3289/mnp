@foreach($buttons as $button)

    <?php $html = $button->render();

    $html = str_replace('fa fa-pencil', 'fas fa-pen-alt', $html);
    echo $html;
    ?>
@endforeach
@if($model && $model->deleted_at)
    <span class="model_deleted" style="display:none"></span>
@endif

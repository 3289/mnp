<?php

namespace App\Http\Sections;

use App\Models\Fap;
use App\Models\Mnp;
use App\Models\Region;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Faps
 *
 * @property \App\Models\Fap $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Faps extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Медорганизации';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        /** @var User $user */
        $user = Auth::user();
        $display = \AdminDisplay::datatables();

        if (!$user->isSuperModer()){
            $region_id = $user->region_id;
            $display->setApply(function ($query) use ($region_id) {
                $query->where(['region_id'=>$region_id]);
            });
        }
        $display
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                \AdminColumn::text('id', '#')->setWidth('30px'),
                \AdminColumn::link('name', 'Имя'),
                \AdminColumn::custom('Статус', function (Fap $fap) {
                    return $fap->getTextStatusRus();
                }),
                \AdminColumn::text('address', 'Адрес'),
                \AdminColumn::relatedLink('region.name', 'Регион')

            )->setColumnFilters([
                null,
                \AdminColumnFilter::text('name', 'name')->setPlaceholder('Имя')->setColumnName('name')
                    ->setOperator(\SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface::CONTAINS),
                \AdminColumnFilter::select()->setPlaceholder('Статус')->setColumnName('status')->setOptions([
                    Fap::STATUS_EXIST => 'рабочая',
                    Fap::STATUS_EXIST_EARLY => 'строящаяся',
                    Fap::STATUS_EXIST_NEVER => 'нерабочая',
                ]),
                \AdminColumnFilter::text()->setPlaceholder('Адрес')->setColumnName('address')
                    ->setOperator(\SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface::CONTAINS),
                \AdminColumnFilter::select(new Region(), 'Регион')
                    ->setDisplay('name')->setPlaceholder('Выбор региона')->setColumnName('region_id'),

            ])->setPlacement('panel.heading');

        if (isset($user) && $user->isModer()) {
            $display->setApply(function ($query) use ($user) {
                $query->where('region_id', $user->region_id ? $user->region_id : 0);
            });
        }

        return $display->paginate(100);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        /** @var User $user */
        $user = \Auth::user();

        $fields = [
            \AdminFormElement::text('name', 'Имя')->required(),
            \AdminFormElement::select('status', 'Статус', [
                3 => 'рабочая', 1 => 'строящаяся', 2 => 'нерабочая'
            ])->required(),
            \AdminFormElement::selectajax('mnp_id', 'МНП')
                ->setModelForOptions(Mnp::class)
                ->setSearchUrl(route('mnps.search.ajax'))
                ->setDisplay('full_name')
                ->required(),

            \AdminFormElement::text('lng', 'Долгота')->required(),
            \AdminFormElement::text('lat', 'Широта')->required(),
            \AdminFormElement::ckeditor('description', 'Описание'),
            \AdminFormElement::textarea('phones', 'Телефоны'),
            \AdminFormElement::text('address', 'Адрес'),
            \AdminFormElement::text('note', 'Оснащенность'),

//            \AdminFormElement::text('rating', 'Рейтинг'),
//            \AdminFormElement::datetime('opening_date', 'Дата открытия'),
            \AdminFormElement::images('photos', 'Изображения'),
        ];
        if ($user->isSuperModer()) {
            $fields[] = \AdminFormElement::select('region_id', 'Регион', Region::class)
                ->setDisplay('name');
        } else {
            $region_id = $user->region_id;
            $region = Region::find($region_id);
            $fields[] = \AdminFormElement::select('region_id', 'Регион', [$region_id => $region->name]);
        }
        return \AdminForm::panel()->addBody($fields);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    public function fireCreate()
    {

        return parent::fireCreate(); // TODO: Change the autogenerated stub
    }

}

<?php

namespace App\Http\Sections;

use App\Models\Fap;
use App\Models\Mnp;
use App\Models\Region;
use App\Models\Request;
use App\Models\User;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Requests
 *
 * @property \App\Models\Request $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Requests extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Обращения';

    /**
     * @var string
     */
    protected $alias;


    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        /** @var User $user */
        $user = \Auth::user();
        $display = \AdminDisplay::datatables()->with('author');

        if (!$user->isSuperModer()){
            $region_id = $user->region_id;
            $display->setApply(function ($query) use ($region_id) {
                $query->where(['region_id'=>$region_id]);
            });
        }
        $display->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                \AdminColumn::text('id', '#')->setWidth('10px'),
                \AdminColumn::text('author.email', 'Почта пользователя'),
                \AdminColumn::text('author.phone', 'Телефон пользователя'),
                \AdminColumn::text('mnp_name', 'МНП'),
                \AdminColumn::text('fap_text', 'Состояние МО'),
                \AdminColumn::relatedLink('mnp.name', 'Ссылка на МНП'),
                \AdminColumn::text('status_text', 'Статус'),
                \AdminColumn::text('rating_text', 'Рейтинг'),
                \AdminColumn::text('created_at', 'Дата обращения'),
                \AdminColumn::relatedLink('region.name', 'Регион')
            )->setColumnFilters([
                null,
                \AdminColumnFilter::text('user.email', 'Почта')
                    ->setPlaceholder('Почта')
                    ->setOperator(\SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface::CONTAINS),
                 \AdminColumnFilter::text('user.phone', 'Телефон')
                     ->setPlaceholder('Телефон')
                     ->setOperator(\SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface::CONTAINS),
                \AdminColumnFilter::text('mnp_name', 'mnp_name')->setPlaceholder('МНП')->setColumnName('mnp_name')
                    ->setOperator(\SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface::CONTAINS),
                null,
                null,
                \AdminColumnFilter::select()->setPlaceholder('Статус')->setColumnName('status')->setOptions([
                    5 => 'На рассмотрении', 2 => 'Отклонено', 3 => 'Одобрено'
                ]),
                \AdminColumnFilter::select()->setPlaceholder('Рейтинг')->setColumnName('rating')->setOptions([

                    5 =>'Не задано', 1 => 'Неудовлетворительно', 2 => 'Удовлетворительно',3=>'Отлично'
                ]),
                null,
                \AdminColumnFilter::select(new Region(), 'Регион')
                    ->setDisplay('name')->setPlaceholder('Выбор региона')->setColumnName('region_id'),
            ])->setPlacement('panel.heading');
        return $display->paginate(100);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        return \AdminForm::panel()->addBody([
            \AdminFormElement::select('status', 'Статус', [
                Request::STATUS_PRORESS => 'На рассмотрении',
                Request::STATUS_MODERATE => 'На модерации',
                Request::STATUS_DECLAIM  => 'Отклонено',
                Request::STATUS_APPROVE => 'Одобрено'])
                ->required(),

            \AdminFormElement::selectajax('user_id', 'Пользователь')
                ->setModelForOptions(User::class)
                ->setDisplay('name')
                ->required(),
            \AdminFormElement::select('fap', 'Состояние МО', [
                Fap::STATUS_EXIST_NEVER=>'Отсуствует',
                Fap::STATUS_EXIST=>'Работает',
                Fap::STATUS_EXIST_EARLY=>'Закрыт'])
                ->required(),
            \AdminFormElement::selectajax('mnp_id', 'МНП')
                ->setModelForOptions(Mnp::class)
                ->setDisplay('name'),

            \AdminFormElement::text('mnp_name', 'Название МНП введенное пользователем'),
            \AdminFormElement::select('size', 'Размер МНП', [2 => 'До 100 человек', 1 => 'До 2000 человек']),
            \AdminFormElement::text('address', 'Адрес'),
            \AdminFormElement::text('distance', 'Удаленность')->setReadOnly(true),
            \AdminFormElement::textarea('misc', 'Дополнительная информация'),
            \AdminFormElement::select('rating', 'Оценка МО', [1 => 'Неудовлетворительно', 2 => 'Удовлетворительно', 3 => 'Отлично', 5 =>'Не задано']),
            \AdminFormElement::checkbox('fapRatingCauseService', 'Плохое обслуживание'),
            \AdminFormElement::checkbox('fapRatingCauseEquipment', 'Оборудование отсутствует или устаревшее'),
            \AdminFormElement::checkbox('fapRatingCauseDoctor', 'Отсутствие врача-фельдшера'),


            \AdminFormElement::images('photos', 'Изображения'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}

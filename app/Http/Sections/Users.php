<?php

namespace App\Http\Sections;

use App\Models\Region;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Users
 *
 * @property \App\Models\User $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Users extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Пользователи';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        /** @var User $user */
        $user = Auth::user();
        /** @var DisplayTable $display */
        $display = \AdminDisplay::datatables();
        if (!$user->isSuperModer()) {
            $region_id = $user->region_id;
            $display->setApply(function ($query) use ($region_id) {
                $query->where(['role' => User::ROLE_USER]);
            });
        }
        $display->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                \AdminColumn::text('id', '#')->setWidth('10px'),
                \AdminColumn::link('name', 'Имя'),
                \AdminColumn::text('email', 'Email'),
                \AdminColumn::text('phone', 'Телефон'),
                \AdminColumn::text('role_name', 'Роль')->setOrderable('role')
            )->setColumnFilters([
                null,
                \AdminColumnFilter::text('name', 'name')->setPlaceholder('Имя')->setColumnName('name')
                    ->setOperator(\SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface::CONTAINS),
                \AdminColumnFilter::text('email', 'email')->setPlaceholder('Email')->setColumnName('email')
                    ->setOperator(\SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface::CONTAINS),
                \AdminColumnFilter::text('phone', 'phone')->setPlaceholder('Телефон')->setColumnName('phone')
                    ->setOperator(\SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface::CONTAINS),
                null
            ])->setPlacement('panel.heading');

        if ($user->isSuperModer()) {
            /** @var \SleepingOwl\Admin\Display\Column\Control $control */
            $control = $display->getColumns()->getControlColumn();

            $link = new \SleepingOwl\Admin\Display\ControlLink(function (User $user) {
                return URL::to('/') . '/admin/users/' . $user->id . '/changepass'; // Генерация ссылки
            }, 'Изменить пароль', 0);
            $link->setIcon('fa fa-cog');
            $link->setHtmlAttribute('class', 'btn btn-success');
            $link->hideText();
            $link->setCondition(function (User $model) use ($user) {
                return $user->isSuperModer();
            });

            $control->addButton($link);
        }
        if (isset($user) && $user->isModer()) {
            $display->setApply(function ($query) use ($user) {
                $query->whereIn('role', [User::ROLE_MODER, User::ROLE_USER]);
            });
        }

        return $display->paginate(25);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        /** @var User $user */
        $user = Auth::user();
        $this->title = 'Пользователи';
        $roles = User::getRoles();

        if (isset($user) && $user->isModer()) {
            unset($roles[User::ROLE_SUPERMODER]);
        }

        $fields = [
            \AdminFormElement::text('name', 'Имя')->required(),
            \AdminFormElement::text('email', 'Email')->required()->unique()->addValidationRule('email'),
            \AdminFormElement::text('phone', 'Телефон'),
        ];
        if (isset($user) && $user->isSuperModer()) {
            $fields[] = \AdminFormElement::select('region_id', 'Регион', Region::class)->setDisplay('name');
            $fields[] = \AdminFormElement::select('role', 'Роль', [
                User::ROLE_SUPERMODER => 'Супермодератор',
                User::ROLE_MODER => 'Модератор',
                User::ROLE_USER => 'Пользователь'
            ])->required();
        } else {
            $fields[] = \AdminFormElement::text('role_name', 'Роль')->setReadOnly(true);
        }


        return \AdminForm::panel()->addBody($fields);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $user = Auth::user();
        $this->title = 'Пользователи';
        $roles = User::getRoles();

        if (isset($user) && $user->isModer()) {
            unset($roles[User::ROLE_SUPERMODER]);
        }

        $fields = [
            \AdminFormElement::text('name', 'Имя')->required(),
            \AdminFormElement::text('email', 'Email')->required()->unique()->addValidationRule('email'),
            \AdminFormElement::text('password', 'Пароль')->required()->setHtmlAttribute('value', '')
                ->mutateValue(function ($value) {
                    return Hash::make($value);
                }),
            \AdminFormElement::text('phone', 'Телефон'),
            \AdminFormElement::select('role', 'Роль', $roles)->required(),
        ];

        if (isset($user) && $user->isSuperModer()) {
            $fields[] = \AdminFormElement::select('region_id', 'Регион', Region::class)->setDisplay('name');
        }

        return \AdminForm::panel()->addBody($fields);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}

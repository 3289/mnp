<?php

namespace App\Http\Sections;

use App\Models\Mnp;
use App\Models\Region;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Mnps
 *
 * @property \App\Models\Mnp $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Mnps extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'МНП';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $user = Auth::user();
        $display = \AdminDisplay::datatables()->setApply(function ($query) {
            $query->whereRaw('district_id!=0');
        });
//        Итак, слева направо фильтры как идут:
//- Название (кстати, "имя МНП" неправильно, правильно "название МНП", поменять в названии поля и названии столбца)
//- Население: от - До (можно убрать, если не лезет, это неважное поле по сути)
//- Ближайшая МО (поле текстового поиска, чтобы можно было найти все МНП с ближайшей МО такой-то)
//- Расстояние: от - До (так же работает, как и сейчас по населению)
//- Время: от - До (так же работает, как и сейчас по населению)
//- Выбор региона (как есть сейчас, просто он справа)
        $display->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                \AdminColumn::text('id', '#')->setWidth('30px'),
                \AdminColumn::text('name', 'Название'),
                \AdminColumn::text('population', 'Население'),
                \AdminColumn::relatedLink('district.name', 'Муниципальное образование'),
                \AdminColumn::relatedLink('nearestFap.name', 'Ближайшая МО'),
                \AdminColumn::text('distance', 'Расстояние, км'),
                \AdminColumn::text('time', 'Время в пути, мин.'),
                \AdminColumn::relatedLink('region.name', 'Регион')
            )
            ->setColumnFilters([
                null,
                \AdminColumnFilter::text('name', 'name')->setPlaceholder('Название')->setColumnName('name')
                    ->setOperator(\SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface::CONTAINS),
                \AdminColumnFilter::range()->setFrom(
                    \AdminColumnFilter::text()->setPlaceholder('От')
                )->setTo(
                    \AdminColumnFilter::text()->setPlaceholder('До')
                )->setColumnName('population'),
                null,
//                \AdminColumnFilter::select(new Mnp(), 'Муниципальное образование')
//                    ->setDisplay('name')->setPlaceholder('Муниципальное образование')->setColumnName('district_id')->setCallback(function($text,$query) {
//                        $query->whereRaw('district_id=0');
//                    }),
                \AdminColumnFilter::text('nearestFap.name', 'Ближайшая МО')
                    ->setPlaceholder('Ближайшая МО')
                    ->setOperator(\SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface::CONTAINS),

                \AdminColumnFilter::range()->setFrom(
                    \AdminColumnFilter::text()->setPlaceholder('От')
                )->setTo(
                    \AdminColumnFilter::text()->setPlaceholder('До')
                )->setColumnName('distance'),
                \AdminColumnFilter::range()->setFrom(
                    \AdminColumnFilter::text()->setPlaceholder('От')
                )->setTo(
                    \AdminColumnFilter::text()->setPlaceholder('До')
                )->setColumnName('time'),
                \AdminColumnFilter::select(new Region(), 'Регион')
                    ->setDisplay('name')->setPlaceholder('Выбор региона')->setColumnName('region_id')
            ])->setPlacement('panel.heading');
        if (isset($user) && $user->isModer()) {
            $display->setApply(function ($query) use ($user) {
                $query->where('region_id', $user->region_id ? $user->region_id : 0);
            });
        }

        return $display->paginate(100);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $this->title = 'МНП';
        $mnp = Mnp::find($id);
        $fields = [
            \AdminFormElement::text('name', 'Имя')->required(),
            \AdminFormElement::text('population', 'Население')->required(),
            \AdminFormElement::text('lat', 'Широта')->required(),
            \AdminFormElement::text('lng', 'Долгота')->required(),

            \AdminFormElement::text('distance', 'Расстояние, км')->setReadOnly(true),
            \AdminFormElement::text('time', 'Время в пути, мин.')->setReadOnly(true),

        ];

        if ($mnp->nearest) {
            $fap = $mnp->nearestFap()->first();
            $fields[] = \AdminFormElement::html('<a href="'.route('admin.model.edit',[
                'adminModel'=>'faps',
                'adminModelId'=>$fap->id,
                ]).'">'.$fap->name.'</a>');
        }
        return \AdminForm::panel()->addBody($fields);
    }

    /**
     * @return bool
     */
    public function isCreatable()
    {
        return false;
    }

    public function isDeletable(Model $model)
    {
        return false;
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Sections\Mnps;
use App\Models\Mnp;
use App\Models\User;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        /** @var User $user */
        $user = \Auth::user();

        $q = $request->get('q');
        $mnps = Mnp::where('name', 'like', '%' . $q . '%')->limit(50);
        if ($user && !$user->isSuperModer()) {
            $region_id = $user->region_id;
            $mnps = $mnps->where(['region_id'=>$region_id]);
        }

        $mnps = $mnps->get();
        $data = [];
        /** @var Mnp $mnp */
        foreach ($mnps as $mnp) {
            $data[] = [
                'tag_name' => $mnp->getFullNameAttribute(),
                'title' => $mnp->getFullNameAttribute(),
                'id' => $mnp->id,
                'lat' => $mnp->lat,
                'lng' => $mnp->lng,
            ];
        }
        return response()->json($data);
    }
}

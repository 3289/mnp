<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Routing\Controller;

class AuthController extends Controller
{
    use ResetsPasswords;

    /**
     * API login
     * curl -d "email=123@ad.ru&password=1234562" -X POST http://adsats.local/api/login
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
        $credentials = $this->getCredentials($request);

        array_splice($credentials, 2);
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {

                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }


        /** @var User $user */
        $user = User::where('email', '=', $credentials['email'])->first();
        $user->api_token = $token;
        $user->save();
        // all good so return the token
        return response()->json([
            'token' => $token,
            'id' => $user->id,
            'name' => $user->name,
            'phone' => $user->phone,
            'place' => $user->place,
            'email' => $user->email,
        ]);
    }

    protected function getAllowFields()
    {
        return ['email', 'password', 'name', 'phone', 'place'];
    }

    /**
     * API Register
     * curl -d "email=123@ad.ru&password=1234562&name=test&first_name=first&last_name=last&phone=123" -X POST http://adsats.local/api/register
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $credentials = $this->getCredentials($request);

        $rules = [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|max:255',
            'name' => 'required|max:255',
            'phone' => 'max:255',
        ];
        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()]);
        }

        $email = $credentials['email'];
        $password = $credentials['password'];
        $name = $credentials['name'];
        $phone = $credentials['phone'];


        $user = User::create([
            'email' => $email,
            'password' => Hash::make($password),
            'name' => $name,
            'phone' => $phone,
            'role' => User::ROLE_USER
        ]);


        $user->password = Hash::make($password);
        $user->save();

        array_splice($credentials, 2);
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {

                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }


        /** @var User $user */
        $user = User::where('email', '=', $credentials['email'])->first();
        $user->api_token = $token;
        $user->save();
        return response()->json([
            'success' => true,
            'token' => $token,
            'email' => $email,
            'name' => $name,
        ]);
    }

    protected function getCredentials(Request $request)
    {

        $allows = $this->getAllowFields();
        return $request->only($allows);
    }


    public function update(Request $request)
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }
        $email = $request->get('email');
        $name = $request->get('name');
        $phone = $request->get('phone');
        $place = $request->get('place');


        if ($email) {

            $user->email = $email;
        }
        if ($name) {

            $user->name = $name;
        }
        if ($phone) {

            $user->phone = $phone;
        }
        if ($place) {

            $user->place = $place;
        }
        $user->save();
        return response()->json(['success' => true,
            'email' => $user->email,
            'phone' => $user->phone,
            'name' => $user->name,
            'place' => $user->place,
        ]);
    }


}
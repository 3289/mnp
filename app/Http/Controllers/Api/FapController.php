<?php

namespace App\Http\Controllers\Api;

use App\Models\Comment;
use App\Models\Fap;
use App\Models\Mnp;
use App\Models\Region;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Routing\Controller;

class FapController extends Controller
{


    public function index(Request $request)
    {

        $regions = [
            28, 29, 30, 31, 33, 34, 35, 75, 7, 40, 9, 42, 43, 44, 23, 45, 47, 49, 50, 51, 52, 53,
            54, 55, 56, 58, 1, 4, 2, 3, 11, 12, 13, 14, 62, 64, 66, 67, 70, 71, 18, 73, 27, 74, 20,
            83, 76];
        $id = $request->get('id');
        $mnps = Mnp::select([
            'mnps.id',
            'mnps.population',
            'mnps.distance',
            'mnps.lat',
            'mnps.lng',
            'mnps.region_id',
            'faps.service',
        ])
            ->leftJoin('faps', 'mnps.id', '=', 'faps.mnp_id')
            ->whereRaw('mnps.district_id!= 0 AND mnps.population<2001 AND mnps.lat IS NOT NULL AND mnps.population!=0')
            ->limit(40000);

        if ($id) {
            $mnps = $mnps->where(['mnps.region_id' => $id]);
        }

        $mnps = $mnps->get();

        $array = [];
        /** @var Mnp $mnp */
        foreach ($mnps as $mnp) {
            /**
             *отдаёшь 0, если расстояние NULL (помнишь, мы не во всех регионах считали расстояния?)
             * отдаёшь 2, если у привязанного к данному МНП ФАП. service = 0 или же расстояние / 40 (аналогично отдаваемому параметру time) > 60
             * отдаёшь 1 в остальных случаях
             *
             * 0 - маленький серый домик, где не было данных о МО (я давал список Идшников, которые были) притом, что население <= 100
             * 1 - большой серый домик - аналогично, только население 101<=X<=2000
             * 2 - маленький зелёный домик - общий случай, что население <= 100
             * 3 - большой зелёный домик - общий случай, население 101<=X<=2000
             * 4 - маленький красный домик - если время >= 60 минут. Пока что это расстояние / 40, но позже, с появлением маршрутов,
             * нужно будет выдаваемое время сравнить с 1 часом. население <= 100
             * 5 - большой красный домик - аналогично, только население 101<=X<=2000
             * 6 - большой зеленый домик с крестом - если в данном МНП есть РАБОЧИЙ МО, в котором не заполнено поле оснащённости note
             * 7 - большой синий домик с крестом - аналогично для СТРОЯЩЕГОСЯ МО
             * 8 - большой красный домик с крестом - если в МНП есть МО и заполнено поле note оснащённости
             *
             * Теперь по приоритетам.
             * Сначала проверяем на условия 0 и 1. Если МНП не в регионах, что я давал списком, то без проверки остальных условий пишем 0 и 1.
             * Затем проверяем есть ли там ФАП, 6 7 8. Если да, то отдаём и дальше не проверяем.
             * Затем проверяем на 4 5. Время больше 60 минут - отдаем 4 5 и дальше не проверяем.
             * В остальных случаях отдаем 2 3.
             */

            $icon_type = 0;
            $time = $mnp->time;
            $fap = null;

            $fap = Fap::where(['mnp_id' => $mnp->id])->first();

            if ($mnp->population <= 100) {
                $icon_type = 2;
            }
            if ($mnp->population > 100) {
                $icon_type = 3;
            }

            if ($time >= 60 && $mnp->population <= 100) {
                $icon_type = 4;
            }
            if ($time >= 60 && $mnp->population > 100) {
                $icon_type = 5;
            }


            if ($fap !== null) {
                if ($fap->isWorking() && $fap->note == null) {
                    $icon_type = 6;
                }
                if ($fap->isProcess() && $fap->note == null) {
                    $icon_type = 7;
                }
                if ($fap->note !== null) {
                    $icon_type = 8;
                }
            }
            if (!in_array($mnp->region_id, $regions)) {

                if ($mnp->population <= 100) {
                    $icon_type = 0;
                }

                if ($mnp->population > 100) {
                    $icon_type = 1;
                }
            }


            $array[] = [
                $mnp->id,
                $icon_type,
                ($mnp->population < 100) ? 0 : 1,
                ($mnp->distance < 6) ? 0 : 1,
                (float)$mnp->lat,
                (float)$mnp->lng,
            ];
        }


        return response()->json($array);
    }

    public function get(Request $request)
    {
        $id = $request->get('id');

        /** @var Mnp $mnp */
        $mnp = Mnp::find($id);

        $fap = Fap::where(['mnp_id' => $id])->first();
        $osnas = null;
        $osnas_score = null;
        $adress = null;
        $phone = null;
        $fap_name = null;
        $fap_description = null;
        $images = [];
        if ($fap == null) {
            $fap = Fap::find($mnp->nearest);
            if ($fap) {
                $adress = $fap->address;
                $phone = json_decode($fap->phones);
                $fap_name = $fap->name;
                $images = json_decode($fap->getPhotosAttribute());
            }
        } else {
            $osnas = $fap->note;
            $osnas_score = $fap->service;
            $adress = $fap->address;
            $phone = json_decode($fap->phones);
            $fap_name = $fap->name;
            $fap_description = $fap->description;
            $images = json_decode($fap->getPhotosAttribute());
        }
        $region = $mnp->region()->first();
        $district = $mnp->district()->first() !== null ? $mnp->district()->first()->name : '';

        $distance = $mnp->time;
        if ($distance !== null) {
            $distance = (float)$distance;
            $hour = floor($distance / 60);
            $min = round($distance % 60);
            $distance = '';
            if ($hour > 0) {
                $distance = $hour . ' ч ';
            }
            $distance = $distance . $min . ' мин';
        }

        $requests = $mnp->requests()->where(['status' => 3])->get()->toArray();
        foreach ($requests as &$comment) {
            $comment['photos'] = json_decode($comment['photos']);
            $user = User::find($comment['user_id']);
            $comment['user_id'] = $user->name;
        }
        unset($comment);
        return response()->json([
            'id' => $mnp->id,
            'name' => $mnp->name,
            'description' => $fap_description,
            'type' => $mnp->getTextStatus(),
            'region' => $region->name,
            'address' => $adress,
            'phone' => $phone,
            'district' => $district,
            'population' => $mnp->population,
            'distance' => round($mnp->distance) . ' км',
            'osnas' => $osnas,
            'osnas_score' => $osnas_score,
            'nearestMed' => $fap_name,
            'time' => $distance,
            'place' => $mnp->name,
            'position' => [
                'lat' => $mnp->lat,
                'lng' => $mnp->lng,
            ],
            'images' => $images,
            'requests' => $requests
        ]);

    }

    public function addComments(Request $request)
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        $message = $request->get('comment');
        $fap_id = $request->get('fap_id');
        if (mb_strpos($fap_id, '_')) {
            $fap_id = explode('_', $fap_id);
            $fap_id = end($fap_id);
        }
        $images = $request->file('photos');

        $credentials = $request->only('comment', 'fap_id');

        $rules = [
            'comment' => 'required|max:255',
            'fap_id' => 'required|',
        ];
        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()]);
        }


        $comment = new Comment();
        $comment->id_author = $user->id;
        $comment->id_fap = $fap_id;
        $comment->message = $message;
        $comment->save();
        $photos = [];
        if (!empty($images)) {
            /** @var UploadedFile $image */
            foreach ($images as $image) {
                $imageName = microtime() . '.' . $image->getClientOriginalExtension();
                $path = '/uploads/comment/' . $comment->id . '/';
                $image->move(public_path($path), $imageName);
                $path = $path . $imageName;
                $photos[] = $path;
            }
        }
        $comment->photos = json_encode($photos);
        $comment->save();
        return response()->json($comment->toArray());

    }

    public function sendRequest(Request $request)
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        $fap = $request->get('fap');
        if ($fap === 'never') {
            $fap = 1;
        }
        if ($fap === 'have') {
            $fap = 3;
        }
        if ($fap === 'closed') {
            $fap = 2;
        }


        $mnp_name = $request->get('mnp');
        $mnp_id = $request->get('mnpId');
        $region_id = $request->get('regionId');
        $type = $request->get('radio');
        if ($type === 'mnpTo100') {
            $type = 2;
        }
        if ($type === 'mnpTo2k') {
            $type = 1;
        }
        $rating = $request->get('rating');
        $address = $request->get('address');
        $distance = $request->get('distance');
        $misc = $request->get('misc');
        $fapRatingCauseService = $request->get('fapRatingCauseService') == 'true' ? 1 : 0;
        $fapRatingCauseEquipment = $request->get('fapRatingCauseEquipment') == 'true' ? 1 : 0;
        $fapRatingCauseDoctor = $request->get('fapRatingCauseDoctor') == 'true' ? 1 : 0;

        $images = $request->file('photos');


//        $rules = [
//            'comment' => 'required|max:255',
//            'fap_id' => 'required|',
//        ];
//        $validator = Validator::make($credentials, $rules);
//        if ($validator->fails()) {
//            return response()->json(['success' => false, 'error' => $validator->messages()]);
//        }

        $req = new \App\Models\Request();
        $req->user_id = $user->id;
        $req->fap = $fap;
        if ($region_id) {
            $req->region_id = $region_id;
        }
        $req->mnp_id = $mnp_id;
        $req->mnp_name = $mnp_name;
        $req->size = $type;
        $req->status = \App\Models\Request::STATUS_PRORESS;
        $req->misc = $misc;
        $req->rating = $rating!==null ? $rating : \App\Models\Request::RATING_PRORESS;
        $req->address = $address;
        $req->distance = $distance;
        $req->fapRatingCauseService = $fapRatingCauseService;
        $req->fapRatingCauseEquipment = $fapRatingCauseEquipment;
        $req->fapRatingCauseDoctor = $fapRatingCauseDoctor;
        $req->save();
        $photos = [];
        if (!empty($images)) {
            /** @var UploadedFile $image */
            foreach ($images as $image) {
                $imageName = microtime() . '.' . $image->getClientOriginalExtension();
                $path = '/uploads/requests/' . $req->id . '/';
                $image->move(public_path($path), $imageName);
                $path = $path . $imageName;
                $photos[] = $path;
            }
        }
        $req->photos = json_encode($photos);
        $req->save();


        return response()->json($req->toArray());
    }

    public function getMyRequest(Request $request)
    {
        try {
            /** @var User $user */
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }

        $data = $user->requests()->get()->toArray();

        return response()->json($data);

    }


    public function byType(Request $request)
    {
        $type = $request->get('type');
        if ($type == 'fap') {
            $faps = Fap::all();
            $array = [];
            foreach ($faps as $fap) {

                $array[] = [
                    $fap->id,
                    $fap->getNumberStatus(),
                    (float)$fap->lat,
                    (float)$fap->lng,
                ];
            }
            return response()->json($array);
        }

        if ($type == 'mnpTo100') {
            $mnps = Mnp::whereRaw('population<=100 AND lat IS NOT NULL')->get();
            $array = [];
            /** @var Mnp $mnp */
            foreach ($mnps as $mnp) {

                $array[] = [
                    $mnp->id,
                    $mnp->distance,
                    (float)$mnp->lat,
                    (float)$mnp->lng,

                ];
            }
            return response()->json($array);
        }
        if ($type == 'mnpTo2k') {
            $mnps = Mnp::whereRaw('population<=2000 AND population>100 AND lat IS NOT NULL')->get();
            $array = [];
            /** @var Mnp $mnp */
            foreach ($mnps as $mnp) {

                $array[] = [
                    $mnp->id,
                    $mnp->distance,
                    (float)$mnp->lat,
                    (float)$mnp->lng,
                ];
            }
            return response()->json($array);
        }
    }

    /**"JSON(object with fields:
     * type(string, one of [mnpTo2k,mnpTo100]),
     * name(string),
     * place(string),
     * population(int),
     * nearest fap(int),
     * rating(int, one of [1,2,3]),
     * comments(array of objects with fields:
     * author(string),
     * date(int timestamp),
     * message(string)))" */

    public function getMnpInfo(Request $request)
    {
        $id = $request->get('id');
        /** @var Mnp $mnp */
        $mnp = Mnp::find($id);
        if ($mnp === null) {
            abort(404);
        }
        $fap = Fap::where(['mnp_id' => $id])->first();

        if ($fap == null) {
            $fap = Fap::whereRaw('1=1')->first();
        }
        $region = $mnp->region()->first();
        $district = $mnp->district()->first();
        return response()->json([
            'type' => $mnp->population > 100 ? 'mnpTo2k' : 'mnpTo100',
            'name' => $mnp->name,
            'region' => isset($region) ? $region->name : '',
            'district' => isset($district) ? $district->name : '',
            'place' => $mnp->name,
            'population' => $mnp->population,
            'nearest' => $fap->id,
            'fap_name' => $fap->name,
            'rating' => $fap->rating,
            'comments' => $fap->comments()->with('author')->get()->toArray()
        ]);
    }

    /**"JSON(object with fields:
     * type(string, one of [fapWorking,fapInProcess,fapNotWorking]),
     * name(string),
     * description(string),
     * openingDate(int timestamp),
     * place(string),
     * phones(array of strings-formated phones),
     * photos(array of strings - urls),
     * rating(int, one of [1,2,3]),
     * comments(array of objects with fields:
     * author(string),
     * date(int timestamp),
     * message(string)))" */
    public function getFapInfo(Request $request)
    {
        $id = $request->get('id');
        $fap = Fap::find($id);
        if ($fap === null) {
            abort(404);
        }
        $photos = [];
        /** @var Comment $comment */
        foreach ($fap->comments()->get() as $comment) {
            $arr = json_decode($comment->photos);
            if (!empty($arr)) {
                foreach ($arr as $item) {
                    $photos[] = $item;
                }
            }
        }
        $phones = [];
        if ($fap->phones != '') {
            $phones = explode("\n", $fap->phones);
        }
        $mnp = $fap->mnp()->first();
        $region = $district = null;
        if ($mnp) {
            $region = $mnp->region()->first();
            $district = $mnp->district()->first();
        }
        return response()->json([
            'type' => $fap->getTextStatus(),
            'name' => $fap->name,
            'description' => strip_tags($fap->description),

            'region' => isset($region) ? $region->name : '',
            'district' => isset($district) ? $district->name : '',

            'openingDate' => strtotime($fap->opening_date),
            'place' => isset($fap->address) ? $fap->address : '',
            'phones' => $phones,
            'rating' => $fap->rating,
            'photos' => $photos,
            'comments' => $fap->comments()->with('author')->get()->toArray()
        ]);
    }


    public function getregions()
    {

        $regions = Region::all();

        $array = [];
        /** @var Region $region */
        foreach ($regions as $region) {
            $array[] = [
                'id' => $region->id,
                'title' => $region->name,
            ];
        }
        return response()->json($array);
    }

}
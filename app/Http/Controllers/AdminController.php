<?php

namespace App\Http\Controllers;

use AdminSection;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    /**
     * Отображает страницу для смены пароля
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function password()
    {
        return AdminSection::view(view('admin.changepass'), 'Изменить пароль');
    }

    /**
     * Меняет пароль пользователя
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePass($id, Request $request)
    {
        $user = User::find($id);
        $user->password = Hash::make($request->input('password'));
        $user->save();
        Session::flash('success_message', 'Пароль успешно изменен!');

        return redirect('admin/users/'.$id.'/edit');
    }
}

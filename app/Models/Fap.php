<?php

namespace App\Models;

use Hamcrest\Thingy;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Fap extends Model
{
    const STATUS_EXIST = 3;
    const STATUS_EXIST_EARLY = 1;
    const STATUS_EXIST_NEVER = 2;

    protected $fillable = [
        'name',
        'status', 'lat', 'lng',
        'description', 'address', 'place',
        'phones', 'photos', 'rating', 'opening_date',
        'region_id', 'note', 'service'];


    public function getTextStatus()
    {
        if ($this->status === self::STATUS_EXIST) {
            return 'fapWorking';
        }
        if ($this->status === self::STATUS_EXIST_EARLY) {
            return 'fapInProcess';
        }
        if ($this->status === self::STATUS_EXIST_NEVER) {
            return 'fapNotWorking';
        }

    }

    public function getNumberStatus()
    {
        if ($this->status === self::STATUS_EXIST) {
            return 0;
        }
        if ($this->status === self::STATUS_EXIST_EARLY) {
            return 1;
        }
        if ($this->status === self::STATUS_EXIST_NEVER) {
            return 2;
        }

    }

    public function getTextStatusRus()
    {
        if ($this->status === self::STATUS_EXIST) {
            return 'рабочая';
        }
        if ($this->status === self::STATUS_EXIST_EARLY) {
            return 'строящаяся';
        }
        if ($this->status === self::STATUS_EXIST_NEVER) {
            return 'нерабочая';
        }

    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    public function mnp()
    {
        return $this->belongsTo('App\Models\Mnp');
    }

    public function getPhotosAttribute()
    {
        return $this->attributes['photos'];
    }


    public function setPhotosAttribute($value)
    {
        $this->attributes['photos'] = json_encode($value);
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment', 'id_fap');
    }

    public function isWorking()
    {
        return $this->status == static::STATUS_EXIST;
    }

    public function isProcess()
    {
        return $this->status == static::STATUS_EXIST_EARLY;
    }

    public function isNotWoking()
    {
        return $this->status == static::STATUS_EXIST_NEVER;
    }
}

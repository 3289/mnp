<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{


    protected $fillable = [
        'name',
        'id_author',
        'mnp_id',
        'message',
        'photos'
    ];


    public function author()
    {
        return $this->belongsTo('App\Models\User','id_author');
    }
    public function mnp()
    {
        return $this->belongsTo('App\Models\Mnp','mnp_id');
    }

    public function setPhotosAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['photos'] = '';
        }
        $this->attributes['photos'] = $value;
    }
}

<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;


    const ROLE_SUPERMODER = 1;
    const ROLE_MODER = 2;
    const ROLE_USER = 3;
    /**
     *
     * 1. Супермодератор (суперадминистратор). Это не один пользователь, а как минимум три. Максимальный уровень ответственности. Им доступны все слои и все объекты, в т.ч. скрытые. Могут наделять функциями модератора региона (и снимать их) любого зарегистрированного пользователя из числа региональных активистов ОНФ для работы с обращениями обычных пользователей данного конкретного региона. Могут при необходимости отключать/включать публичное отображение отдельных слоёв карты.
     * 2. Модератор (администратор). Принимает обращения пользователей и вносит данные в систему для одного назначенного ему супермодератором конкретного региона. В регионе их может быть несколько (это зависит от размера региона). Могут вносить данные индекса здоровья региона.
     * 3. Обычные зарегистрированные пользователи. Могут посылать обращения и оставлять комментарии с фотографиями, как в карточке объекта МНП, так и у ФАП. Не могут видеть закрытые / нерабочие ФАП (как и незарегистрированные пользователи).
     */


    protected $fillable = [
        'name', 'email', 'password', 'phone', 'role', 'place','api_token'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    static function getRoles()
    {
        return [
            User::ROLE_SUPERMODER => 'Супермодератор',
            User::ROLE_MODER => 'Модератор',
            User::ROLE_USER => 'Пользователь',
        ];
    }

    public function getRole()
    {
        $roles = static::getRoles();
        return  $roles[$this->attributes['role']];
    }

    public function requests()
    {
        return $this->hasMany('App\Models\Request');
    }

    public function scopeModer($query)
    {
        return $query->whereIn('role', [self::ROLE_MODER, self::ROLE_SUPERMODER]);
    }

    public function isModer()
    {
        return ($this->attributes['role'] == self::ROLE_MODER);
    }

    public function isSuperModer()
    {
        return ($this->attributes['role'] == self::ROLE_SUPERMODER);
    }

    public function getRoleNameAttribute(){
        $roles = static::getRoles();
        return  $roles[$this->attributes['role']];
    }

}

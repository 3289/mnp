<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Mnp extends Model
{


    protected $fillable = [
        'id',
        'name',
        'population',
        'indent',
        'region_id',
        'region_tmp',
        'district_id',
        'lat',
        'lng',
        'nearest',
        'distance',
        'time',
    ];


    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    public function district()
    {
        return $this->belongsTo('App\Models\Mnp');
    }

    public function getTextStatus()
    {

        if ($this->population <= 100) {
            return 4;
        }
        return 3;
    }

    public function getFullNameAttribute()
    {

        $rayon = $this->district()->first();
        if ($rayon) {
            $rayon = $rayon->name . ', ';
        } else {
            $rayon = '';
        }
        return $this->name . ', ' . $rayon . $this->region()->first()->name;
    }

    public function getNameWithRegAttribute()
    {
        return $this->name . ', ' . $this->region()->first()->name;
    }


    public function fap()
    {
        return $this->belongsTo('App\Models\Fap', 'mnp_id');
    }

    public function nearestFap()
    {
        return $this->belongsTo('App\Models\Fap', 'nearest');
    }


    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function requests()
    {
        return $this->hasMany('App\Models\Request');
    }

}

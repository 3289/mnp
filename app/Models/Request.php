<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{

    const  STATUS_PRORESS = 5;//'На рассмотрении'
    const  STATUS_MODERATE = 1;//'На модерации'
    const  STATUS_DECLAIM = 2;//'Отклонено'
    const  STATUS_APPROVE = 3;//'Одобрено'

    const  RATING_PRORESS = 5;//'Нe задан'
    const  RATING_DECLAIM = 1;//'Неудовлетворительно'
    const  RATING_APPROVE = 2;//'Удовлетворительно'
    const  RATING_GOOD = 3; //Отлично


    protected $fillable = [
        'user_id',
        'fap',
        'mnp_id',
        'region_id',
        'mnp_name',
        'status',
        'size',
        'rating',
        'address',
        'distance',
        'misc',
        'fapRatingCauseService',
        'fapRatingCauseEquipment',
        'fapRatingCauseDoctor',
        'photos'
    ];


    public function author()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function mnp()
    {
        return $this->belongsTo('App\Models\Mnp', 'mnp_id');
    }


    public function setPhotosAttribute($value)
    {

        if (is_array($value)) {
            $this->attributes['photos'] = json_encode($value);
            return;
        }
        if (empty($value)) {
            $this->attributes['photos'] = '';
        }
        $this->attributes['photos'] = $value;
    }

    public function getFapTextAttribute()
    {
        if ($this->fap == Fap::STATUS_EXIST) {
            return 'Работает';
        }
        if ($this->fap ==  Fap::STATUS_EXIST_NEVER) {
            return 'Отсуствует';
        }
        if ($this->fap ==  Fap::STATUS_EXIST_EARLY) {
            return 'Закрыт';
        }
        return 'Не задан';
    }

    public function getStatusTextAttribute()
    {
        if ($this->status == static::STATUS_DECLAIM) {
            return 'Отклонено';
        }
        if ($this->status == static::STATUS_APPROVE) {
            return 'Одобрено';
        }
        return 'На рассмотрении';
    }

    public function getSizeTextAttribute()
    {
        if ($this->size == 2) {
            return 'До 100 человек';
        }
        return 'До 2000 человек';
    }

    public function getRatingTextAttribute()
    {
        if ($this->rating == static::RATING_DECLAIM) {
            return 'Неудовлетворительно';
        }
        if ($this->rating == static::RATING_APPROVE) {
            return 'Удовлетворительно';
        }
        if ($this->rating == static::RATING_GOOD) {
            return 'Отлично';
        }
        return 'Не задано';
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

}

<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{



    protected $fillable = [
        'name',
        'code',
        'index'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

}

<?php

namespace App\Console\Commands;

use App\Models\Fap;
use App\Models\Mnp;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class LoadFap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:fap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $faps = Fap::whereRaw('lat is null and lng is null and mnp_id is null and region_id is not null')->get();
        $this->api = new \Yandex\Geo\Api();
        foreach ($faps as $fap) {
            $name = $fap->name;
            $region_name = $fap->region()->first()->name;

            $alternative_name = str_replace('д.', 'деревня ', $name);
            $alternative_name = str_replace('д ', 'деревня ', $alternative_name);
            $alternative_name = str_replace('с.', 'село ', $alternative_name);
            $alternative_name = str_replace('с ', 'село ', $alternative_name);
            $alternative_name = str_replace('п.', 'поселок ', $alternative_name);
            $alternative_name = str_replace('п ', 'поселок ', $alternative_name);
            $alternative_name = str_replace('станция', 'станции', $alternative_name);

            $q = $alternative_name . ' ' . $region_name;

            $res = $this->getGeo($q);
            if ($res) {
                $check_result = $this->check($fap, $res);
                if ($check_result){
                    continue;
                }
            }
            ///деревня
            if (strpos('деревня', $alternative_name) !== false) {
                $q = $alternative_name . ' ' . $region_name;
                $q = str_replace('деревня', 'село', $q);
                $res = $this->getGeo($q);
                if ($res) {
                    $check_result = $this->check($fap, $res);
                    if ($check_result){
                        continue;
                    }
                }
                $q = str_replace('село', 'поселок', $q);
                $res = $this->getGeo($q);
                if ($res) {
                    $check_result = $this->check($fap, $res);
                    if ($check_result){
                        continue;
                    }
                }
            }

            ///село
            if (strpos('село', $alternative_name) !== false) {
                $q = $alternative_name . ' ' . $region_name;
                $q = str_replace('село', 'деревня', $q);
                $res = $this->getGeo($q);
                if ($res) {
                    $check_result = $this->check($fap, $res);
                    if ($check_result){
                        continue;
                    }
                }
                $q = str_replace('деревня', 'поселок', $q);
                $res = $this->getGeo($q);
                if ($res) {
                    $check_result = $this->check($fap, $res);
                    if ($check_result){
                        continue;
                    }
                }
            }

            ///поселок
            if (strpos('поселок', $alternative_name) !== false) {
                $q = $alternative_name . ' ' . $region_name;
                $q = str_replace('поселок', 'село', $q);
                $res = $this->getGeo($q);
                if ($res) {
                    $check_result = $this->check($fap, $res);
                    if ($check_result){
                        continue;
                    }
                }
                $q = str_replace('село', 'деревня', $q);
                $res = $this->getGeo($q);
                if ($res) {
                    $check_result = $this->check($fap, $res);
                    if ($check_result){
                        continue;
                    }
                }
            }
        }


    }

    public function check(Fap $fap, $position)
    {

        $count_district = Mnp::where($position)
            ->where('district_id', '=', '0')
            ->count();
        if ($count_district > 0) {
            return false;
        }
        $mnp = Mnp::where($position)->first();

        if (!$mnp) {
            return false;
        }
        $fap->lat = $position['lat'];
        $fap->lng = $position['lng'];
        $fap->mnp_id = $mnp->id;
        $fap->save();
        return true;
    }

    public function getGeo($q)
    {
        $this->api->setQuery($q);

        // Настройка фильтров
        $this->api->setLimit(3)// кол-во результатов
        ->setLang(\Yandex\Geo\Api::LANG_RU)// локаль ответа
        ->load();

        $response = $this->api->getResponse();
        // кол-во найденных адресов
        if ($response->getFoundCount() == 0) {
            return false;
        }

        $lat = $response->getLatitude(); // широта для исходного запроса
        $lng = $response->getLongitude(); // долгота для исходного запроса
        if (!$lat || !$lng) {

            $collection = $response->getList();
            foreach ($collection as $item) {
                $lat = $item->getLatitude(); // широта
                $lng = $item->getLongitude(); // долгота
                break;
            }
        }
        return [
            'lat' => $lat,
            'lng' => $lng
        ];
    }
}

<?php

namespace App\Console\Commands;

use App\Models\Fap;
use App\Models\Mnp;
use App\Models\Region;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class LoadFapOsnash extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:fap:osn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $handle = fopen('osn.csv', "r");
        while (($data = fgetcsv($handle)) !== FALSE) {


            $region = trim($data[0]);

            $name = trim($data[1]);

            $api = new \Yandex\Geo\Api();
            $q = $region . ' ' . $name;
            $api->setQuery($q);

            // Настройка фильтров
            $api->setLimit(3)// кол-во результатов
            ->setLang(\Yandex\Geo\Api::LANG_RU)// локаль ответа
            ->load();

            $response = $api->getResponse();
            // кол-во найденных адресов
            if ($response->getFoundCount() == 0) {
                dump('found many',$q);
                dump($response->getData());
                continue;
            }

            $lat = $response->getLatitude(); // широта для исходного запроса
            $lng = $response->getLongitude(); // долгота для исходного запроса
            if (!$lat || !$lng) {

                $collection = $response->getList();
                foreach ($collection as $item) {
                    $lat = $item->getLatitude(); // широта
                    $lng = $item->getLongitude(); // долгота
                    break;
                }
            }

            $fap = Fap::where(['lat' => $lat, 'lng' => $lng])->first();
            if($fap===null){
                dump('not found',$data,$lat,$lng);
                $fap=   $this->createFap($name,$lat,$lng);
            }
            $fap->note = $data[2];
            $fap->service = 0;
            $fap->save();
            dump('!!!!!!!!!!!!!!!!!!!!!---------------------SAVE------------------------!!!!!!!!!');
        }


    }
    public function createFap($name, $lat, $lng)
    {
        $fap = new Fap();

        $fap->mnp_id = 0;
        $fap->name = $name;
        $fap->lat = $lat;
        $fap->lng = $lng;
        $fap->status = Fap::STATUS_EXIST;
        $fap->save();
        return $fap;
    }


}

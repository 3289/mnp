<?php

namespace App\Console\Commands;

use App\Models\Mnp;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Readers\LaravelExcelReader;
use Symfony\Component\Finder\SplFileInfo;

class MnpParseExcel extends Command
{
    private $district = null;
    private $last = null;
    private $indent = 1;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:mnp:excel {path=excel/}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->argument('path');

        $files = File::allFiles($path);

        /** @var SplFileInfo $file */
        foreach ($files as $file)
        {
            Excel::load($file->getPathname(), function($reader) {
                $this->district = null;
                $this->last = null;
                $this->indent = 1;
                /** @var LaravelExcelReader $reader */
                $reader = $reader;
                $rowIterator = $reader->excel->getActiveSheet()->getRowIterator();

                while ($rowIterator->valid()) {
                    $cellIterator = $rowIterator->current()->getCellIterator('A', 'B');
                    $rowInfo = $this->getRowInfo($cellIterator);

                    if ($rowInfo == []) {
                        $rowIterator->next();
                        continue;
                    }

                    $district_id = $this->getDistrictId($rowInfo);

//                    $mnp = Mnp::where('name', '=', $rowInfo['name'])->first();
//                    if ($mnp == null) {
                        $mnp = new Mnp();
                        $mnp->name = $rowInfo['name'];
                        $mnp->indent = $rowInfo['indent'];
                        $mnp->population = $rowInfo['all'];
                        $mnp->region_tmp = $reader->getFileName();
                        $mnp->district_id = 0;
//                    }
                    $mnp->save();

                    if ($rowInfo['indent'] > $this->indent) {
                        $this->district = $this->last;
                    } else if ($rowInfo['indent'] == $this->indent && $district_id != 0 && $this->district->id = $this->last->id) {
                        $this->district = $this->last->district;
                    }
                    $this->last = $mnp;


                    $this->indent = $rowInfo['indent'];

                    $rowIterator->next();
                }
            });
        }
    }

    private function getRowInfo($cellIterator)
    {
        $rowInfo = [];
        while ($cellIterator->valid()) {
            /** @var \PHPExcel_Cell $cell */
            $cell = $cellIterator->current();
            if ($cell->getColumn() == 'A') {
                if ($cell->getStyle()->getAlignment()->getIndent() === 0 || $cell->getValue() == null
                    || strpos($cell->getValue(), 'Все население') !== false
                    || strpos($cell->getValue(), 'Городское население') !== false
                    || strpos($cell->getValue(), 'Сельское население') !== false) {
                    return [];
                }

                $rowInfo['name'] = $cell->getValue();
                $rowInfo['indent'] = $cell->getStyle()->getAlignment()->getIndent();
            }
            if ($cell->getColumn() == 'B') {
                if (is_numeric($cell->getValue())) {
                    $rowInfo['all'] = $cell->getValue();
                } else {
                    $rowInfo['all'] = 0;
                }
            }

            $cellIterator->next();
        }

        return $rowInfo;
    }

    private function getDistrictId($rowInfo)
    {
        if ($rowInfo['indent'] == $this->indent) {
            if (isset($this->district)) {
                return $this->district->id;
            } else {
                return 0;
            }
        }

        if ($rowInfo['indent'] > $this->indent) {
            if (isset($this->last)) {
                return $this->last->id;
            } else {
                return 0;
            }
        }

        if (!isset($this->district->district)) {
            return 0;
        }

        if (($this->district->indent - $rowInfo['indent']) >= 1) {
            while (isset($this->district->district) && $rowInfo['indent'] > $this->district->district->indent-1) {
                $this->district = $this->district->district;
            }
            if (!isset($this->district->district)) {
                return $this->district->id;
            }
        }

        return $this->district->district->id;
    }
}

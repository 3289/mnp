<?php

namespace App\Console\Commands;

use App\Models\Mnp;
use Illuminate\Console\Command;

class FixMnp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:mnp:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public
        $arr =
        [
//            "район",
//            "cельское поселение",
//            "сельский округ",
//            "городской округ",
//            "сельсовет",
//            "муниципальный район",
"район",
'городской округ',
//'улус',
'кожуун',
'муниципальный округ',
'муниципальный район',
        ];

    public $next = [

        'подчиненные',
'подчиненными',
'район',
'городской округ',
'улус',
'кожуун',
'муниципальный округ',
'муниципальный район',
'cельское поселение',
'сельский округ',
'сельсовет',
    ];



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mnps = Mnp::where('population', '<', '2001')
        ->where(['district_id'=>'0'])->orderBy('id');
        foreach ($this->next as $item) {
            $mnps->where('name', 'NOT LIKE', '%' . $item . '%');
        }
        $mnps = $mnps->get();
        foreach ($mnps as $mnp) {

            $id = $this->getParent($mnp);

            if($id==null){
               continue;
            }
            $mnp->district_id= $id;

            $mnp->save();
        }
    }

    private function getParent(Mnp $mnp)
    {
        $id = $mnp->id;
        $parent = Mnp::whereRaw('id <'.$id)->orderBy('id','DESC')->first();

        if($parent==null){
            return 0;
        }


        if($this->strposa($parent->name, $this->arr)){
            return $parent->id;
        }
        return $this->getParent($parent);

    }

    function strposa($haystack, $needles=array()) {
        $chr = array();
//        dd($haystack, $needles);
        foreach($needles as $needle) {
            if (mb_strlen($haystack) - mb_strlen($needle) === mb_strrpos($haystack,$needle)){
                $chr[$needle] = 1;
            }
        }
//        dd($haystack, $needles);
        if(empty($chr)) return false;
        return true;
    }
}

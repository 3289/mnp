<?php

namespace App\Console\Commands;

use App\Models\Mnp;
use Illuminate\Console\Command;

class MnpParse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:mnp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Yandex\Geo\Exception
     * @throws \Yandex\Geo\Exception\CurlError
     * @throws \Yandex\Geo\Exception\ServerError
     */
    public function handle()
    {
//https://geocode-maps.yandex.ru/1.x/?geocode=деревня%20Бочатино,%20Бокситогорский%20район,%20Ленинградская%20область

        $mnps = Mnp::whereRaw('id<300')
            ->get();

        /** @var Mnp $mnp */
        foreach ($mnps as $mnp) {
//$mnp = Mnp::find(74);
//dd($mnp->getFullNameAttribute());
            $api = new \Yandex\Geo\Api();
//            dd($mnp->getFullNameAttribute());


            $q = $mnp->getFullNameAttribute();
            $q = str_replace('станция','станции',$q);
            $api->setQuery($q);

// Настройка фильтров
            $api
                ->setLimit(3)// кол-во результатов
                ->setLang(\Yandex\Geo\Api::LANG_RU)// локаль ответа
                ->load();

            $response = $api->getResponse();
            // кол-во найденных адресов
            if ($response->getFoundCount() == 0) {
                continue;
            }

            $lat = $response->getLatitude(); // широта для исходного запроса
            $lng = $response->getLongitude(); // долгота для исходного запроса
            if (!$lat) {

                $collection = $response->getList();
                foreach ($collection as $item) {
                    $lat = $item->getLatitude(); // широта
                    $lng = $item->getLongitude(); // долгота
                    break;
                }
            }


            $parent = $mnp->district()->first();
            if ($parent !== null && $parent->lat == $lat && $parent->lng = $lng) {
                $q = $mnp->getNameWithRegAttribute();
                $q = str_replace('станция','станции',$q);
                $api->setQuery($q);

                $api->setLimit(3)// кол-во результатов
                    ->setLang(\Yandex\Geo\Api::LANG_RU)// локаль ответа
                    ->load();

                $response = $api->getResponse();
                // кол-во найденных адресов
                if ($response->getFoundCount() > 1) {
                    continue;
                }

                $lat = $response->getLatitude(); // широта для исходного запроса
                $lng = $response->getLongitude(); // долгота для исходного запроса
                if (!$lat) {

                    $collection = $response->getList();
                    foreach ($collection as $item) {
                        $lat = $item->getLatitude(); // широта
                        $lng = $item->getLongitude(); // долгота
                        break;
                    }
                }
            }

            $mnp->lat = $lat; // широта
            $mnp->lng = $lng; // долгота
            $mnp->save();
        }

    }

    public function getPoint($point, Mnp $mnp, $parent = null)
    {
        $lat = $point->geometry->location->lat;
        $lng = $point->geometry->location->lng;
        $mnp->lat = $lat;
        $mnp->lng = $lng;

        if ($parent == null) {

            $current = $point->address_components[0]->short_name;
        }
        foreach ($point->address_components as $address) {
            if ($address->types[0] !== 'locality') {
                continue;
            }

            if ($parent === null) {
                $parent = $address->short_name;
                /** @var Mnp $parent */
                $parent = Mnp::where(['short_name' => $parent])->first();
            }

            if ($parent) {

                $mnp->district_id = $parent->id;
            }

        }
        $mnp->short_name = $current;


        $mnp->save();
    }
}

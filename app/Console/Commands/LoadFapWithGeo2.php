<?php

namespace App\Console\Commands;

use App\Models\Fap;
use App\Models\Mnp;
use App\Models\Region;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class LoadFapWithGeo2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:fapgeo2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = File::allFiles('FAP_NEW/1/');


        $rayons = [
            'район',
            'городской округ',
            'кожуун',
            'муниципальный округ',
            'муниципальный район',
        ];
        /** @var \SplFileInfo $file */
        foreach ($files as $file) {
            $handle = fopen($file->getRealPath(), "r");
            $region_id = $file->getFilename();
            $region_id = explode('.', $region_id);
            $region_id = (int)reset($region_id);

            $region = Region::find($region_id);

            $region_name = isset($region) ? $region->name : '';

            $district = null;
            while (($data = fgetcsv($handle)) !== FALSE) {
                $data[1] = isset($data[1]) ? $data[1] : '';
                $data[2] = isset($data[2]) ? $data[2] : '';
                $data[3] = isset($data[3]) ? $data[3] : '';
                $data[4] = isset($data[4]) ? $data[4] : '';
                $data[5] = isset($data[5]) ? $data[5] : '';
                $data[6] = isset($data[6]) ? $data[6] : '';


                if ($data[0] !== '' && $data[1] == '' && $data[2] == '' && $data[3] == ''
                    &&(
                        (mb_strpos($data[0],$rayons[0])!==false)
                        || (mb_strpos($data[0],$rayons[1])!==false)
                        || (mb_strpos($data[0],$rayons[2])!==false)
                        || (mb_strpos($data[0],$rayons[3])!==false)
                        || (mb_strpos($data[0],$rayons[4])!==false)
                    )
                ) {
                    $district = trim($data[0], '"');
                }

                if (
                    ($data[1] == '' || $data[1] == '0' || $data[1] == '-' || $data[1] == 'Нет' || $data[1] == 'нет') &&
                    ($data[2] == '' || $data[2] == '0' || $data[2] == '-' || $data[2] == 'Нет' || $data[2] == 'нет') &&
                    ($data[3] == '' || $data[3] == '0' || $data[3] == '-' || $data[3] == 'Нет' || $data[3] == 'нет') &&
                    ($data[4] == '' || $data[4] == '0' || $data[4] == '-' || $data[4] == 'Нет' || $data[4] == 'нет') &&
                    ($data[5] == '' || $data[5] == '0' || $data[5] == '-' || $data[5] == 'Нет' || $data[5] == 'нет') &&
                    ($data[6] == '' || $data[6] == '0' || $data[6] == '-' || $data[6] == 'Нет' || $data[6] == 'нет')
                ) {
                    continue;
                }

                $name = trim($data[0]);
                if ($name == '') {
                    continue;
                }
                $name = str_replace('пгт.', 'пгт ', $name);
                if (strpos('район', $name)) {
                    $name = explode('район', $name);
                    $name = end($name);
                    $name = trim($name);
                }

                if (strpos('р-он', $name)) {
                    $name = explode('р-он', $name);
                    $name = end($name);
                    $name = trim($name);
                }

                $alternative_name = str_replace('д.', 'деревня ', $name);
                $alternative_name = str_replace('д ', 'деревня ', $alternative_name);
                $alternative_name = str_replace('с.', 'село ', $alternative_name);
                $alternative_name = str_replace('с ', 'село ', $alternative_name);
                $alternative_name = str_replace('п.', 'поселок ', $alternative_name);
                $alternative_name = str_replace('п ', 'поселок ', $alternative_name);
                $alternative_name = str_replace('станция', 'станции', $alternative_name);

                $fap = Fap::where('name', $alternative_name)->first();
                if (isset($fap)) {
                    $fap->region_id = $region_id;
                    $fap->save();
                    continue;
                }

                $api = new \Yandex\Geo\Api();
                $q = $district . ' ' . $alternative_name . ' ' . $region_name;
                $api->setQuery($q);

                // Настройка фильтров
                $api->setLimit(3)// кол-во результатов
                ->setLang(\Yandex\Geo\Api::LANG_RU)// локаль ответа
                ->load();

                $response = $api->getResponse();
                // кол-во найденных адресов
                if ($response->getFoundCount() == 0) {
                    continue;
                }

                $lat = $response->getLatitude(); // широта для исходного запроса
                $lng = $response->getLongitude(); // долгота для исходного запроса
                if (!$lat || !$lng) {

                    $collection = $response->getList();
                    foreach ($collection as $item) {
                        $lat = $item->getLatitude(); // широта
                        $lng = $item->getLongitude(); // долгота
                        break;
                    }
                }

                $this->createFap($alternative_name, $lat, $lng, $region_id);
            }


        }


    }

    public function createFap($name, $lat, $lng, $region_id)
    {
        $fap = new Fap();

        $fap->mnp_id = 0;
        $fap->region_id = $region_id;
        $fap->name = $name;
        $fap->lat = $lat;
        $fap->lng = $lng;
        $fap->status = Fap::STATUS_EXIST;
        $fap->save();
    }
}

<?php

use SleepingOwl\Admin\Navigation\Page;

AdminSection::addMenuPage(\App\Models\User::class)
    ->setTitle('Пользователи')
    ->setPriority(2)
    ->setIcon('fa fa-user');
AdminSection::addMenuPage(\App\Models\Mnp::class)
    ->setTitle('МНП')
    ->setPriority(3)
    ->setIcon('fa fa-home');
AdminSection::addMenuPage(\App\Models\Fap::class)
    ->setTitle('Медорганизации')
    ->setPriority(4)
    ->setIcon('fa fa-briefcase-medical');
AdminSection::addMenuPage(\App\Models\Request::class)
    ->setTitle('Обращения')
    ->setPriority(5)
    ->setIcon('fa fa-envelope');

<?php

Route::get('', ['as' => 'admin.dashboard', function () {
	$content = '';
	return redirect('admin/requests');
}]);

Route::get('information', ['as' => 'admin.information', function () {
	$content = 'Define your information here.';
	return AdminSection::view($content, 'Information');
}]);

Route::get('users/{id}/changepass', ['uses' => '\App\Http\Controllers\AdminController@password']);
Route::post('users/{id}/changepass', ['uses' => '\App\Http\Controllers\AdminController@changePass']);
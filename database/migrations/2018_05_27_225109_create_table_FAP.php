<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFAP extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('status');
            $table->integer('mnp_id');
            $table->integer('lat');
            $table->integer('lng');
            $table->text('description')->nullable(true);
            $table->text('address')->nullable(true);
            $table->text('phones')->nullable(true);
            $table->text('photos')->nullable(true);
            $table->float('rating')->nullable(true);
            $table->dateTime('opening_date')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faps');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableComments extends Migration
{


  public function up()
    {

        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_author');
            $table->integer('id_fap');
            $table->text('message');
            $table->text('photos')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRequestsTableData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('requests')
            ->where('size', 0)
            ->update(['size' => 2]);
        DB::table('requests')
            ->where('status', 0)
            ->update(['status' => 2]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('requests')
            ->where('size', 2)
            ->update(['size' => 0]);
        DB::table('requests')
            ->where('status', 2)
            ->update(['status' => 0]);
    }
}

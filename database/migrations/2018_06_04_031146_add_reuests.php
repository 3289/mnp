<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReuests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('fap');
            $table->integer('mnp_id')->nullable(true);
            $table->string('mnp')->nullable(true);
            $table->integer('size');
            $table->integer('status')->default(0);
            $table->integer('rating')->nullable(true);
            $table->text('address')->nullable(true);
            $table->integer('distance')->nullable(true);
            $table->tinyInteger('fapRatingCauseService')->nullable(true);
            $table->tinyInteger('fapRatingCauseEquipment')->nullable(true);
            $table->tinyInteger('fapRatingCauseDoctor')->nullable(true);
            $table->text('photos')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}

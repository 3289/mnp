<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mnps', function (Blueprint $table) {
            $table->float('distance')->nullable(true);
            $table->integer('nearest')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mnps', function (Blueprint $table) {
            $table->dropColumn('nearest');
            $table->dropColumn('distance');
        });
    }
}

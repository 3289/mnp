<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RequestRenameMnp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests', function(Blueprint $table) {
            $table->renameColumn('mnp', 'mnp_name');
        });
    }


    public function down()
    {
        Schema::table('requests', function(Blueprint $table) {
            $table->renameColumn('mnp_name', 'mnp');
        });
    }
}

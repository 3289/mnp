<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGeo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('faps', function (Blueprint $table) {
            $table->float('lat')->nullable(true)->change();
            $table->float('lng')->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faps', function (Blueprint $table) {
            $table->string('lat')->nullable(true)->change();
            $table->string('lng')->nullable(true)->change();
        });
    }
}

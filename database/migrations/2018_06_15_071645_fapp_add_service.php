<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FappAddService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('faps', function (Blueprint $table) {
            $table->text('note')->nullable();
            $table->integer('service')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faps', function (Blueprint $table) {
            $table->dropColumn('note');
            $table->dropColumn('service');
        });
    }
}
